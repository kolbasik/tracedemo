﻿using System;
using System.Diagnostics;

namespace TraceDemo
{
    class Program
    {
        static void Main(string[] args)
        {
            if (Log.On)
            {
                Log.Write(TraceSources.Demo, TraceEventType.Start, "!Start");
            }

            Log.Write(TraceSources.Demo, TraceEventType.Verbose, "!Verbose");
            Log.Write(TraceSources.Demo, TraceEventType.Information, "!Information");
            Log.Write(TraceSources.Demo, TraceEventType.Warning, "!Warning");
            Log.Write(TraceSources.Demo, TraceEventType.Error, "!Error");
            Log.Write(TraceSources.Demo, TraceEventType.Critical, "!Critical");
            
            if (Log.On)
            {
                Log.Write(TraceSources.Demo, TraceEventType.Stop, "!Stop");
            }

            Console.WriteLine("Press any key...");
            Console.ReadKey(true);
        }
    }

    internal static class TraceSources
    {
        public static readonly TraceSource Demo = new TraceSource("Demo", SourceLevels.Information);
    }

    public static class Log
    {
        static Log()
        {
#if DEBUG
            On = true;
#endif
        }

        public static bool On { get; internal set; }

        public static void Write(TraceSource traceSource, TraceEventType eventType, string format, params object[] args)
        {
            traceSource.TraceEvent(eventType, 0, format, args);
        }
    }
}
